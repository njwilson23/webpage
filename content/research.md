# Research interests

<img class="inline" src="images/melt_pond_reflection_small.jpg">

I am a glaciologist/oceanographer studying several aspects of ocean-ice
interactions, including measurement and attribution of melt rates under ice
shelves, the propagation of variability between polar oceans and ice sheets, and
the sensitivity of ocean-ice systems to external forcing.

By developing a better understanding of the ocean and ice dynamics at marine
glaciers, we can learn more about how changes in the ocean may alter the
physical landscape of glacial coastlines, ecosystems and patterns of biological
productivity, and rates of sea level rise. The physics of ice-ocean interactions
are challenging because there are important processes across many scales of time
and space requiring consideration, and because good data is difficult to obtain.

<!-- I approach these challenges by ... -->

In the past I have been lucky to be involved in glacier-related work in
Argentina, Alaska, Yukon, Nunavut, and Greenland. My Master's thesis focused on
using ice-penetrating radar and modelling methods to probe the heat content of
alpine glaciers in the St. Elias Mountains, and how temperature and melt water
distributions can be used to interpret glacier dynamics and climate history.

## Publications

<ul>

  <li><p class="pubitem">
  Wilson, N. J., and F. Straneo (2015). Water exchange between the continental
  shelf and the cavity beneath Nioghalvfjerdsbræ (79 North Glacier), Geophysical
  Research Letters., 42, doi:10.1002/2015GL064944.
  <a href="http://onlinelibrary.wiley.com/doi/10.1002/2015GL064944/abstract">(Abstract)</a>
  </p></li>

  <li><p class="pubitem">
  Schoof, C.,  C. A. Rada, N. J. Wilson, G. E. Flowers, and M. Haseloff (2014).
  Oscillatory subglacial drainage in the absence of surface melt. The
  Cryosphere. Vol 8, pages 959-976. doi:10.5194/tc-8-959-2014. <a
  href="http://www.the-cryosphere.net/8/959/2014/tc-8-959-2014.html">(Article)</a>
  </p></li>

  <li><p class="pubitem">Wilson, N. J., G. E. Flowers, and L. Mingo (2013).
  Comparison of thermal stricture and evolution between neighbouring
  subarctic glaciers. Journal of Geophysical Research. Vol 118.
  doi:10.1002/jgrf.20096.
  <a href="http://onlinelibrary.wiley.com/doi/10.1002/jgrf.20096/abstract">(Abstract)</a>
  </p></li>
  
  <li><p class="pubitem">Wilson, N. J. and G. E. Flowers (2013).
  Environmental controls on the thermal structure of alpine glaciers. The
  Cryosphere. doi:10.5194/tc-7-167-2013.
  <a href="http://www.the-cryosphere.net/7/167/2013/tc-7-167-2013.html">(Article)</a>
  </p></li>
  
  <li><p class="pubitem">Thomson, S. N., M. T. Brandon, J. H. Tomkin, P. W.
  Reiners, C. Vásquez, and N. J. Wilson (2010). Glaciation as a destructive
  and constructive control on mountain building. Nature Letters. 467, 7313.
  doi:10.1038/nature09365.
  <a href="http://www.nature.com/nature/journal/v467/n7313/full/nature09365.html">(Abstract)</a>
  </p></li>

</ul>

<img class="inline" src="images/ice_ravine_small.jpg">

