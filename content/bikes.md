# Bikes

I have a powder blue single-speed based on a Schwinn Le Tour frame from the
late seventies, and a forest green touring bike built on a Brodie Argus frame I
found hanging in a bike shop.

The single-speed ("Clotilde") has carried me in various forms for 9 or 10 years
now. Currently, it has a 27" wheel up front and a 700c wheel in the back, with
46t Sugino Messenger cranks, and a 17t freewheel.

<img class="inline" src="images/bike-clotilde.jpg">

Meanwhile, my touring bike ("Whillans") has 700c wheels and a (untouring-like)
2x8 drivetrain with friction shifters and disc brakes.

<img class="inline" width=500 src="images/bike-whillans.jpg">

# Some places I've ridden to

**Alaska**

Talkeetna, Frosty Bottom Race (Anchorage)

**British Columbia**

Nanaimo, Denman Island, Hornby Island, Galiano Island

**Washington**

Bellingham, San Juan Island, Lopez Island, Orcas Island

