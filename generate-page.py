#! /usr/bin/env python
#
# This is a dirt-simple script for generating a static site
#
# Individual pages are coded as dictionaries in the `sitecontents` list
#

from jinja2 import Environment, FileSystemLoader
import markdown as md
import codecs

env = Environment(loader=FileSystemLoader("templates"))
basic_template = env.get_template("page.html")
katex_template = env.get_template("page_katex.html")

sitecontents = [{"html": "research.html",
                 "markdown": "content/research.md",
                 "template": basic_template},

                {"html": "melting.html",
                 "markdown": "content/melting.md",
                 "template": katex_template},

                #{"html":"bikes.html",
                # "markdown":"content/bikes.md"},
                
                ]

for source in sitecontents:

    print("reading {0}".format(source["markdown"]))
    with codecs.open(source["markdown"], "r", encoding="utf-8") as f:
        content = f.read()

    print("\tconverting...")
    template = source.get("template", basic_template)
    html = template.render(content=md.markdown(content))

    print("\twriting {0}...".format(source["html"]))
    with codecs.open(source["html"], "w", encoding="utf-8") as f:
        f.write(html)

    print("\t...done!")

